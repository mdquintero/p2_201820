/**
 * Autores: Mateo David Quintero Reyes, Daniel Armando Babativa Aparicio
 */
package model.data_structures;

import java.util.Iterator;

import model.vo.Trip;

/**
 * Implementacion de una cola generica
 * Esta implementacion se hizo en base al material trabajado en clase
 * @param <T> Objeto generico
 */

public class Queue<T> implements IQueue<T> {


	//ATRIBUTOS
	private int size;
	private Node<T> first, last;


	//CONSTRUCTOR(ES)
	public Queue(){
		first=null;
		last=null;
		size=0;
	}


	//M�TODOS
	@Override
	public ListIterator<T> iterator(){
		ListIterator<T> listIt = new ListIterator<T>();
		listIt.setCurrent(first);
		return listIt;
		
	}


	@Override
	public boolean isEmpty() {
		return (size==0);
	}


	@Override
	public int size() {
		return size;
	}

	@Override
	public void enqueue(T t) {
		Node<T> newNode = new Node<T>(t, null);
		if(size==0){
			first=newNode;
			last=newNode;
		}
		last.setNextNode(newNode);
		last=newNode;
		size++;

	}

	@Override
	public T dequeue() {
		if(size==0){
			System.out.println("No hay elementos en la cola");
			return null;
		}
		else if(first==last){
			T item = first.getItem();
			first=null;
			last=null;
			size--;
			return item;
		}
		else{
			Node<T> oldFirst=first;
			T item = first.getItem();
			first=oldFirst.getNext();
			oldFirst.setNextNode(null);
			size--;
			return item;
		}
	}

	public Node<T> getLast(){
		return last;
	}

}
