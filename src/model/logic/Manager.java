package model.logic;
import model.vo.ResultadoCampanna;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.nio.file.FileSystemNotFoundException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import com.google.gson.JsonArray;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.opencsv.CSVReader;

import API.IManager;
import model.data_structures.LinearProbingTH;
import model.data_structures.LinkedList;
import model.data_structures.ListIterator;
import model.data_structures.Queue;
import model.data_structures.RedBlackBST;
import model.data_structures.SeparateChainingTH;
import model.data_structures.Sorting;
import model.data_structures.Stack;
import model.vo.Bike;
import model.vo.Route;
import model.vo.Sector;
import model.vo.Station;
import model.vo.Trip;
import model.vo.Trip.Tipo;

public class Manager implements IManager {
	public final static String rutaGeneral="."+File.separator+"data"+File.separator;
	//Ruta del archivo de datos 2017-Q1
	// TODO Actualizar
	public static final String TRIPS_Q1 = rutaGeneral+"Divvy_Trips_2017_Q1.csv";

	//Ruta del archivo de trips 2017-Q2
	// TODO Actualizar	
	public static final String TRIPS_Q2 = rutaGeneral+"Divvy_Trips_2017_Q2.csv";

	//Ruta del archivo de trips 2017-Q3
	// TODO Actualizar	
	public static final String TRIPS_Q3 = rutaGeneral+"Divvy_Trips_2017_Q3.csv";

	//Ruta del archivo de trips 2017-Q4
	// TODO Actualizar	
	public static final String TRIPS_Q4 = rutaGeneral+"Divvy_Trips_2017_Q4.csv";

	//Ruta del archivo de trips 2017-Q1-Q4
	// TODO Actualizar	
	public static final String TRIPS_Q1_Q4 = "Ruta Trips 2017-Q1-Q4 en directorio data";

	//Ruta del archivo de stations 2017-Q1-Q2
	// TODO Actualizar	
	public static final String STATIONS_Q1_Q2 = rutaGeneral+"Divvy_Stations_2017_Q1Q2.csv";

	//Ruta del archivo de stations 2017-Q3-Q4
	// TODO Actualizar	
	public static final String STATIONS_Q3_Q4 = rutaGeneral+"Divvy_Stations_2017_Q3Q4.csv";

	private static RedBlackBST<Date, LinkedList<Trip>> arbolTrips = new RedBlackBST<Date, LinkedList<Trip>>();

	private RedBlackBST<Integer, Bike> arbolBikes;

	private RedBlackBST<Integer, Bike> arbolBikesTiempo;

	private static SeparateChainingTH<Integer,Station> hashStations = new SeparateChainingTH<Integer,Station>(); 

	/**
	 * Crea una Linear Probing TH para los trips donde la llave es el número del grupo
	 */
	private static LinearProbingTH<Integer,LinkedList<Trip>> hashTrips = new LinearProbingTH<Integer,LinkedList<Trip>>();

	private static LinearProbingTH<String,RedBlackBST<Integer,LinkedList<Trip>>> hashEstaciones = new LinearProbingTH<String, RedBlackBST<Integer,LinkedList<Trip>>>();

	private Stack<Route> stackRoute = new Stack<Route>();

	private Sector[][] sectores;

	private int xSectores;
	private int ySectores;

	private double longMinima = 0;

	private double longMax=0;

	private double latMinima=0;

	private double latMax=0;

	private Trip[] arregloTrips;

	private LinkedList<Trip> listaTrips;

	private LinkedList<Station> listaStations;

	private RedBlackBST<Double, Station> arbolEstaciones;
	// TODO Actualizar	
	public static final String BIKE_ROUTES = rutaGeneral+"CDOT_Bike_Routes_2014_1216-transformed(1).json";


	@Override
	public void cargarDatos(String rutaTrips, String rutaStations, String dataBikeRoutes) {
		listaTrips = new LinkedList<Trip>();
		listaStations = new LinkedList<Station>();
		String[] estaciones = rutaStations.split(":");
		String[] trips = rutaTrips.split(":");
		//CARGAR STATIONS

		for(int j=0; j<estaciones.length; j++)
		{
			try{
				FileReader fr = new FileReader(new File(estaciones[j]));
				CSVReader br = new CSVReader(fr);

				String[] line = br.readNext();
				//Se avanza de nuevo porque la línea anterior no contiene datos
				line = br.readNext();

				while(line!=null)
				{
					String[] fecha = (line[6].replace(' ', '/').replace(':', '/')).split("/");
					int tamano = fecha.length;
					Integer[]fecha2 = new Integer[tamano];
					for(int i = 0; i< fecha2.length; i++)
					{
						fecha2[i] = Integer.parseInt(fecha[i]);
					}
					LocalDateTime ini = LocalDateTime.of(fecha2[2], fecha2[0], fecha2[1],fecha2[3],fecha2[4],tamano==6 ? fecha2[5]:00);
					Station s = new Station(Integer.parseInt(line[0]), line[1],ini, Double.parseDouble(line[3]),Double.parseDouble(line[4]), Integer.parseInt(line[5]));


					hashStations.put(s.getStationId(), s);
					listaStations.add(s);
					line = br.readNext();
				}

				br.close();
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}

		//CARGA TRIPS
		for(int j=0; j<1; j++)
			//for(int j=0; j<trips.length; j++)
		{
			try{
				FileReader fr = new FileReader(new File(trips[j]));
				CSVReader br = new CSVReader(fr);
				Trip t=null;


				String[] line = br.readNext();
				//Se avanza de nuevo porque la línea anterior no contiene datos
				line = br.readNext();
				while(line!=null)
				{

					String[] fechaInicio = (line[1].replace(' ', '/').replace(':', '/')).split("/");
					int tamano = fechaInicio.length;
					Integer[]fechaInicio2 = new Integer[tamano];
					String[] fechaFin = (line[2].replace(' ', '/').replace(':', '/')).split("/");
					Integer[]fechaFin2 = new Integer[tamano];

					for(int i = 0; i< fechaFin2.length; i++)
					{
						fechaInicio2[i] = Integer.parseInt(fechaInicio[i]);
						fechaFin2[i] = Integer.parseInt(fechaFin[i]);
					}

					LocalDateTime ini = LocalDateTime.of(fechaInicio2[2], fechaInicio2[0], fechaInicio2[1],fechaInicio2[3],fechaInicio2[4],(tamano==6 ? fechaInicio2[5]:00));
					LocalDateTime fini = LocalDateTime.of(fechaFin2[2], fechaFin2[0], fechaFin2[1],fechaFin2[3],fechaFin2[4],tamano==6 ? fechaFin2[5]:00);
					Tipo tipo = null;

					if(line[9].equals("Subscriber"))
					{
						tipo = Tipo.SUBSCRIBER;
					} else
					{
						tipo = Tipo.CUSTOMER;
					}

					if(line[10]==null || line[10].equals(""))
					{
						t = new Trip(Integer.parseInt(line[0].replace("\"","")),  ini , fini , Integer.parseInt(line[3].replace("\"","")), 
								Integer.parseInt(line[4].replace("\"","")), Integer.parseInt(line[5].replace("\"","")), Integer.parseInt(line[7].replace("\"","")), Trip.UNKNOWN, tipo);

						double dist = calcularDistancia(t.getStartStationId(), t.getEndStationId());
						t.setDistance(dist);
					}
					else{
						t = new Trip(Integer.parseInt(line[0].replace("\"","")),  ini , fini , Integer.parseInt(line[3].replace("\"","")), 
								Integer.parseInt(line[4].replace("\"","")), Integer.parseInt(line[5].replace("\"","")), Integer.parseInt(line[7].replace("\"","")), line[10], tipo);

						double dist = calcularDistancia(t.getStartStationId(), t.getEndStationId());
						t.setDistance(dist);
					}
					Date finiDate = conversorLocalToDate(fini);
					//asigna los nombres de las estaciones inicio y fin al trip
					int IDStart = t.getStartStationId();
					int IDEnd = t.getEndStationId();
					String inicio = hashStations.get(IDStart).getStationName();
					String end = hashStations.get(IDEnd).getStationName();
					t.setStart(inicio);
					t.setEnd(end);
					//Verifica si dada una fecha hay una lista asociada a esta, en caso de que no, la crea, le agrega el trip y agrega al árbol
					LinkedList<Trip> aux = arbolTrips.get(finiDate);
					listaTrips.add(t);
					if(aux!=null){
						aux.add(t);
						arbolTrips.put(finiDate, aux);
					}
					else{
						LinkedList<Trip> aux1 = new LinkedList<Trip>();
						aux1.add(t);
						arbolTrips.put(finiDate, aux1);
					}

					//Calcula el tiempo en minutos de un viaje
					double tiempMin = (t.getTripDuration()/60);
					int tiempMinInt = (int) tiempMin + 1;
					if(tiempMinInt%2 !=0 ){
						tiempMinInt ++;
					}
					//Verifica si dado el número del grupo de un viaje hay una lista asociada a esta, en caso de que no, la crea
					//le agrega el trip y agrega a la tabla de hash
					LinkedList<Trip> aux2 = hashTrips.get(tiempMinInt/2);

					if(aux2!=null){
						aux2.add(t);
						hashTrips.put(tiempMinInt/2, aux2);
					}
					else{
						LinkedList<Trip> aux3 = new LinkedList<Trip>();
						aux3.add(t);
						hashTrips.put(tiempMinInt/2, aux3);
					}
					line = br.readNext();
				}
				br.close();
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}

		//CARGA DEL JSON
		try
		{
			JsonParser parser = new JsonParser();
			JsonArray inicio = (JsonArray) parser.parse(new FileReader(dataBikeRoutes));
			JsonArray fin = inicio;


			for (int i = 0; fin != null && i < fin.size(); i++)

			{

				JsonObject obj = (JsonObject)fin.get(i);
				String id = "";
				String tipo ="";
				String ruta = "";
				String calleReferencia ="";
				double distancia = 0.0;
				String calleLimiteExtremo1 = "";
				String calleLimiteExtremo2 ="";	

				if(obj!=null)
				{
					if(obj.get("id")!=null)
						id=obj.get("id").getAsString();

					if(obj.get("BIKEROUTE")!=null)
						tipo = obj.get("BIKEROUTE").getAsString();

					if(obj.get("the_geom")!=null)
						ruta = obj.get("the_geom").getAsString();

					if(obj.get("STREET")!=null)
						calleReferencia = obj.get("STREET").getAsString();

					if(obj.get("Shape_leng")!=null)
						distancia = obj.get("Shape_leng").getAsDouble();

					if(obj.get("F_STREET")!=null && !obj.get("F_STREET").isJsonNull())
						calleLimiteExtremo1 = obj.get("F_STREET").getAsString();

					if(obj.get("T_STREET")!=null&& !obj.get("T_STREET").isJsonNull())
						calleLimiteExtremo2 = obj.get("T_STREET").getAsString();
				}

				ruta = ruta.replace("(", ",");
				ruta = ruta.replace(")", ",");
				String[] splits = ruta.split(",");
				Route t = new Route(id, tipo, splits, calleReferencia, calleLimiteExtremo1, calleLimiteExtremo2, distancia);
				for(int u = 1; u<splits.length;u++)
				{
					String[] coordenadas = splits[u].split(" ");
					double longi = 0.0;
					double lati = 0.0;
					if(coordenadas.length==3)
					{
						longi = Double.parseDouble(coordenadas[1]);
						lati = Double.parseDouble(coordenadas[2]);
					} else if (coordenadas.length==2)
					{
						longi = Double.parseDouble(coordenadas[0]);
						lati = Double.parseDouble(coordenadas[1]);
					}
					if(longMax==0)
					{
						longMax = longi;
					} else if(longi>longMax)
					{
						longMax = longi;
					}
					if(longMinima==0)
					{
						longMinima = longi;
					}else if(longi<longMinima)
					{
						longMinima = longi;
					}
					if(latMax==0)
					{
						latMax = lati;
					} else if(lati>latMax)
					{
						latMax = lati;
					}
					if(latMinima==0)
					{
						latMinima = lati;
					} else if(lati<latMinima)
					{
						latMinima = lati;
					}
				}

				stackRoute.push(t);


			}
		}

		catch (JsonIOException e1 ) {
			e1.printStackTrace();
		}
		catch (JsonSyntaxException e2) {
			e2.printStackTrace();
		}
		catch (FileSystemNotFoundException e3) {
			e3.printStackTrace();
		} catch (FileNotFoundException e) {

			e.printStackTrace();
		}

		System.out.println("Inside loadServices with " + dataBikeRoutes);


		//CARGA DE BICICLETAS

		cargarBikesRedBlackBST();
		cargarHashEstaciones();

	}

	@Override
	public Queue<Trip> A1(int n, LocalDate fechaTerminacion) {
		//lista con todos los trips de esa fecha
		LinkedList<Trip> auxiliar = new LinkedList<Trip>();

		Iterable<Date> llaves = arbolTrips.keys(conversorLocalToDate(fechaTerminacion.atTime(0, 0)), 
				conversorLocalToDate(fechaTerminacion.atTime(23, 59)));
		for(Date date: llaves) {
			LinkedList<Trip> lista = arbolTrips.get(date);
			ListIterator<Trip> iter = lista.iterator();
			while(iter.hasNext()) {
				auxiliar.add(iter.next());
			}
		}

		Queue<Trip> result = new Queue<Trip>();
		//Itera sobre los trips de la fecha y agrega los trips que cumplen la condicion a la cola a retornar
		ListIterator<Trip> iteradorDate = auxiliar.iterator();
		while(iteradorDate.hasNext()) {
			Trip t = iteradorDate.next();
			Station st = hashStations.get(t.getEndStationId());
			if(st.getCapacity() >= n) {
				result.enqueue(t);
			}
		}

		return result;
	}

	@Override
	public LinkedList<Trip> A2(int n) {
		LinkedList<Trip> listaReturn = new LinkedList<Trip>();

		if(n%2 != 0){
			n++;
		}

		LinkedList<Trip> listaTripsGrupo = hashTrips.get(n/2);
		ListIterator<Trip> iter = listaTripsGrupo.iterator();
		while(iter.hasNext()){
			listaReturn.add(iter.next());
		}

		return listaReturn;

	}

	@Override
	public LinkedList<Trip> A3(int n, LocalDate fecha) {

		//lista con todos los trips de esa fecha
		LinkedList<Trip> auxiliar = new LinkedList<Trip>();

		Iterable<Date> llaves = arbolTrips.keys(conversorLocalToDate(fecha.atTime(0, 0)), conversorLocalToDate(fecha.atTime(23, 59)));
		for(Date date: llaves) {
			LinkedList<Trip> lista = arbolTrips.get(date);
			ListIterator<Trip> iter = lista.iterator();
			while(iter.hasNext()) {
				auxiliar.add(iter.next());
			}
		}

		Trip[] aOrdenar = tripsToArray(auxiliar);
		Sorting.ordenar(Sorting.QUICK, aOrdenar ,new ComparadorTripsDistanciaDescendente());

		//lista con los n trips a retornar
		LinkedList<Trip> resultado = new LinkedList<Trip>();
		for(Trip t: aOrdenar){
			resultado.add(t);
			n--;
			if(n==0){
				break;
			}
		}

		return resultado;
	}

	@Override
	public LinkedList<Bike> B1(int limiteInferior, int limiteSuperior) {
		LinkedList<Bike> holaMundo = new LinkedList<Bike>();
		for(int i=limiteInferior; i<=limiteSuperior;i++)
		{
			Bike actual = arbolBikesTiempo.get(i);
			if(actual!=null)
			{

				holaMundo.add(actual);
			}

		}
		return holaMundo;
	}

	@Override
	public LinkedList<Trip> B2	(String estacionInicio, String estacionFinal, int limiteInferiorTiempo, int limiteSuperiorTiempo) 
	{
		LinkedList<Trip> r = new LinkedList<Trip>();
		RedBlackBST<Integer, LinkedList<Trip>> buscador = hashEstaciones.get(estacionInicio+"-"+estacionFinal);
		if(buscador!=null)
		{
			for(int i= limiteInferiorTiempo; i<=limiteSuperiorTiempo; i++)
			{
				LinkedList<Trip> trips = buscador.get(i);
				if(trips!=null)
				{
					for(int j=0;j<trips.size();j++)
					{
						Trip objeto = trips.get(j).getItem();
						r.add(objeto);				
					}
				}

			}
		}
		return r;





	}

	@Override
	public int[] B3(String estacionDeInicio, String estacionDeLlegada) {
		RedBlackBST<Integer, LinkedList<Trip>> arbol = hashEstaciones.get(estacionDeInicio+"-"+estacionDeLlegada);
		int min = 0;		
		int max = 0;
		if(arbol!=null)
		{
			min = arbol.min();
			max = arbol.max();
			System.out.println("Min " + min + " Max " + max);
		}
		int iniMax = 0;
		int finiMax = 0;
		int cantidadMax=0;
		int contador=0;
		if(min>0 && max>0)
		{
			for(int j=0, fini= 1; j<=23; j++, fini++)
			{
				if(fini==24)
				{
					fini=0;
				}
				for(int i = min; i<= max;i++)
				{
					LinkedList<Trip> trips = arbol.get(i);
					if(trips!=null)
					{
						for(int k = 0; k<trips.size();k++)
						{
							Trip tripcito = trips.get(i).getItem();
							int horaInicial = tripcito.getStartTime().getHour();
							int horaFinal = tripcito.getStopTime().getHour();
							boolean inicia = horaInicial==j || horaFinal ==fini;
							System.out.println(inicia);
							boolean finaliza = horaFinal ==j || horaFinal==fini;
							System.out.println(finaliza);
							if(inicia==true || finaliza == true)
							{
								contador++;
								System.out.println(contador);
							}
						}
					}
				}
				if(cantidadMax<contador)
				{
					iniMax=j;
					finiMax=fini;
					cantidadMax=contador;
				}
			}
		}

		return new int[] {iniMax, cantidadMax};
	}

	@Override
	public ResultadoCampanna C1(double valorPorPunto, int numEstacionesConPublicidad, int mesCampanna) {
		ResultadoCampanna r = null;

		if(mesCampanna<12&&1<mesCampanna)
		{
			arbolEstaciones = new RedBlackBST<Double, Station>();
			ListIterator<Station> ite = listaStations.iterator();
			while(ite.hasNext())
			{
				Station actual = ite.next();
				double promedio = promedioPuntos(mesCampanna, actual);
				arbolEstaciones.put(promedio, actual);
			}
			r = new ResultadoCampanna(0,new LinkedList<Station>());
			while(numEstacionesConPublicidad>0)
			{
				double promedioActual = arbolEstaciones.max();
				Station lista = arbolEstaciones.get(promedioActual);
				r.addCosto(promedioActual*valorPorPunto);
				r.addToEstaciones(lista);
				numEstacionesConPublicidad--;
				arbolEstaciones.deleteMax();

			}


			//arboo rojo negro y remove max n veces
		}
		else
		{
			System.out.println("No está dentro del rango, debe ser un numero entre 2 y 11");
		}

		return r;
	}

	public double promedioPuntos(int mes, Station station)
	{
		double suma=0;
		ListIterator<Trip> ite = listaTrips.iterator();
		while(ite.hasNext())
		{
			Trip tripcito = ite.next();
			boolean sus = false;
			int mesActual = tripcito.getStartTime().getMonthValue();
			int mesFinal = tripcito.getStopTime().getMonthValue();
			if(tripcito.getStartStationId()==station.getStationId())
			{
				if(tripcito.getStartTime().getYear()==2017)
				{
					if(mesActual == mes || mesActual == mes-1 || mesActual == mes+1)
					{
						suma++;
					}
				}
				if(tripcito.getTipo() == Tipo.SUBSCRIBER && !sus)
				{
					suma++;
					sus = !sus;
				}
			} 
			if(tripcito.getEndStationId()==station.getStationId())
			{
				if(tripcito.getStopTime().getYear()==2017)
				{
					if(mesFinal == mes || mesFinal == mes-1 || mesFinal == mes+1)
					{
						suma++;
					}
				}
				if(tripcito.getTipo() == Tipo.SUBSCRIBER && !sus)
				{
					suma++;
					sus = !sus;
				}
			}

		}
		return suma/3;
	}

	@Override
	public double[] C2(int LA, int LO) {
		sectores = new Sector[LO][LA];
		xSectores=LO;
		ySectores=LA;

		//tamaño de un sector en longitud
		double diferenciaLongi=((longMax-longMinima)/LO);
		//tamaño de un sector en latitud
		double diferenciaLati=((latMax-latMinima)/LA);

		int contador = 1;
		for(int j = 0; j<LA; j++) {

			for(int i = 0; i<LO; i++) {

				//primer sector de toda la matriz
				if(i==0 && j==0)
				{
					sectores[i][j] = new Sector(longMinima, longMinima + diferenciaLongi, latMinima, latMinima + diferenciaLati, 
							contador);
				}

				//primera fila
				else if(i!=0 && j==0) {
					double pInicialLong=sectores[i-1][0].longFinal()+0.00000000000001;

					sectores[i][j] = new Sector(pInicialLong, pInicialLong + diferenciaLongi, 
							sectores[0][0].latInicial(), sectores[0][0].latFinal(),contador );
				}
				//primera columna
				else if(i==0 && j!=0) {
					double pInicialLat=sectores[0][j-1].latFinal()+0.00000000000001;

					sectores[i][j] = new Sector(sectores[0][0].longInicial(), sectores[0][0].longFinal(), 
							pInicialLat, pInicialLat + diferenciaLati, contador);
				}
				//sector en i!=0 , j!=0
				else {
					double pInicialLong=sectores[i-1][0].longFinal()+0.00000000000001;
					double pInicialLat=sectores[0][j-1].latFinal() + 0.00000000000001;

					sectores[i][j] = new Sector(pInicialLong, pInicialLong + diferenciaLongi,
							pInicialLat, pInicialLat + diferenciaLati, contador );
				}
				contador++;
			}
		}

		sectorizarRutas();
		sectorizarEstaciones();

		double[] arr = new double[]{latMax, longMax, latMinima, longMinima};
		return arr;

	}


	@Override
	public int darSector(double latitud, double longitud) {
		Sector enSecto = enSector(longitud, latitud);
		if(enSecto != null)
		{
			return enSecto.getID();
		}
		return 0;
	}

	@Override
	public LinkedList<Station> C3(double latitud, double longitud) {
		Sector sectorcito = enSector(longitud, latitud);
		LinkedList<Station> r = new LinkedList<Station>();
		if(sectorcito!=null)
		{
			if(r.size()==0) {
				System.out.println("No hay estaciones en el sector resultante");
			}
			else {
				for(int i=0; i<sectorcito.getStations().length;i++)
				{
					r.add(sectorcito.getStations()[i]);
				}
			}
		}
		else {
			System.out.println("La localización dada no pertenece a ningún sector");
		}
		return r;
	}

	@Override
	public LinkedList<Route> C4(double latitud, double longitud) {
		Sector sectorcito = enSector(longitud, latitud);
		LinkedList<Route> r = new LinkedList<Route>();
		if(sectorcito!=null) {
			if(r.size()==0) {
				System.out.println("No hay ciclorutas que atraviesen el sector de consulta");
			}
			else {
				for(int i=0; i<sectorcito.getRutas().length;i++)
				{
					r.add(sectorcito.getRutas()[i]);
				}
			}
		}
		else {
			System.out.println("La localización dada no pertenece a ningún sector");
		}
		return r;
	}

	@Override
	public LinkedList<Route> C5(double latitudI, double longitudI, double latitudF, double longitudF){
		LinkedList<Route> secto1 = C4(latitudI, longitudI);
		ListIterator<Route> sect1 = secto1.iterator();
		LinkedList<Route> secto2 = C4(latitudF, longitudF);

		LinkedList<Route> r = new LinkedList<Route>();

		while(sect1.hasNext())
		{
			System.out.println("entra iterador 1");
			Route ruta1 = sect1.next();

			System.out.println("Streer ruta1: " + ruta1.getCalleReferencia());
			Route existe = null;
			boolean found = false;
			ListIterator<Route> sect2 = secto2.iterator();
			while(sect2.hasNext()&&!found)
			{
				Route ruta2 = sect2.next();
				System.out.println("entra iterator 2");
				System.out.println("Street ruta2: " + ruta2.getCalleReferencia());

				if(ruta1.getID().equals(ruta2.getID()))
				{
					System.out.println("entra al equals");
					found = !found;
					existe = ruta1;
				}
				else
				{
					System.out.println("no entra al equals");
				}
			}

			if(existe!=null)
			{
				System.out.println("Existe un elemento");
				double[] longi = existe.longitudes();
				double[] lati = existe.latitudes();
				long distanciaInicial = 1000000000;
				long distanciaFinal = 1000000000;
				double latInicio = 0;
				double longInicio = 0;
				double latFinal = 0;
				double longFinal = 0;
				for(int x=0; x<longi.length; x++)
				{
					double distancia = calculateDistance(lati[x], longi[x], longitudI, latitudI);
					if(distancia<distanciaInicial)
					{
						distanciaInicial = (long) distancia;
						latInicio = lati[x];
						longInicio = longi[x];
					}

					distancia = calculateDistance(latitudF, longitudF, longi[x], lati[x]);

					if(distancia<distanciaFinal)
					{
						distanciaFinal = (long) distancia;
						latFinal = lati[x];
						longFinal = longi[x];
					}
				}
				double distanciaTotal =(long) distanciaInicial + distanciaFinal + calculateDistance(latFinal, longFinal, longInicio, latInicio);
				existe.setDistanciaC5(distanciaTotal);
				r.add(existe);
			}else
			{
				System.out.println("no existe un elemento");
			}
		}
		return r;
	}




	//METODOS AUXILIARES

	/**
	 * Permite cargar las bicicletas a partir de la informacion de todos los viajes.
	 * Las bicicletas se cargan en un arbol rojo negro.
	 */
	public void cargarBikesRedBlackBST() {
		Trip[] arreglo = allTripsToArray();

		if(arbolBikes==null)
			arbolBikes = new RedBlackBST<>();

		if(arbolBikesTiempo==null)
		{
			arbolBikesTiempo = new RedBlackBST<>();
		}

		if(arreglo!=null) {
			Sorting.ordenar(Sorting.QUICK, arreglo,new ComparadorTripsFecha());

			int idActualBike =0;
			int cantViajes =0;
			double distancia=0.0;
			int duracion=0;
			int i =0;

			boolean cen = false;

			while(i<arreglo.length) {

				Trip actual = arreglo[i];

				idActualBike=actual.getBikeId();

				while(actual.getBikeId()==idActualBike && !cen) {

					cantViajes++;
					distancia+=actual.getDistance();
					duracion+=actual.getTripDuration();

					i++;

					boolean alFinal = (i==arreglo.length);
					if(!alFinal)
						actual=arreglo[i];

					else {
						actual=arreglo[i-1];
						cen=true;
					}
				}

				arbolBikes.put(idActualBike, new Bike(idActualBike, cantViajes, distancia, duracion));
				arbolBikesTiempo.put(duracion, new Bike(idActualBike, cantViajes, distancia, duracion));
				cantViajes=0;
				distancia=0.0;
				duracion=0;
			}
		}
	}

	/**
	 * Crea un arreglo con todos los trips almacenados al cargar.
	 * @return Arreglo con todos los trips almacenados al cargar.
	 */
	public Trip[] allTripsToArray() {
		Queue<Date> colaLlaves = (Queue<Date>) arbolTrips.keys();
		arregloTrips = new Trip[colaLlaves.size()];

		int i=0;
		while(!colaLlaves.isEmpty()){
			LinkedList<Trip> listaTripsDate = arbolTrips.get(colaLlaves.dequeue());
			ListIterator<Trip> iter = listaTripsDate.iterator();
			while(iter.hasNext()){
				arregloTrips[i] = iter.next();
			}
			i++;
		}

		return arregloTrips;
	}

	/**
	 * Crea un arreglo con los trips en una lista pasada por parámetro
	 * @param lista Lista de trips
	 */

	public Trip[] tripsToArray(LinkedList<Trip> lista){
		ListIterator<Trip> iter = lista.iterator();
		Trip[] a = new Trip[lista.size()];

		int i=0;
		while(iter.hasNext()){
			a[i] = iter.next();
			i++;
		}
		return a;
	}



	/**
	 * Calcula la distancia entre dos estaciones
	 * @param inicio Id estacion inicial
	 * @param end Id estacion final
	 * @return Distancia entre dos estaciones
	 */
	public static double calcularDistancia(int inicio, int end)
	{

		Station referencia = hashStations.get(inicio);
		Station ended = hashStations.get(end);


		double x = 0;

		if(referencia != null && ended != null)
		{
			x = calculateDistance(ended.getLat(), ended.getLongitude(), referencia.getLongitude(), referencia.getLat());
		}

		return x;
	}

	/**
	 * Conversor de LocalDateTime a Date
	 * @param a Fecha a convertir
	 * @return Fecha convertida en formato Date
	 */
	public Date conversorLocalToDate(LocalDateTime a){
		return Date.from(a.atZone(ZoneId.systemDefault()).toInstant());
	}


	public void sectorizarRutas()
	{
		Stack<Route> rutas = stackRoute;
		int tamano = rutas.size();
		for(int i=0;i<tamano;i++)
		{
			Route actual = rutas.pop();
			double[] lat = actual.latitudes();
			double[] longi = actual.longitudes();
			for(int j=0; j<lat.length; j++)
			{
				double lati = lat[j];
				double longii = longi[j];
				Sector enSecto = enSector(longii,lati);

				if(enSecto!=null)
				{
					boolean hay = enSecto.hayRuta(actual);
					if(!hay)
					{
						enSector(longii,lati).addRuta(actual);
					}
				}


			}


		}
	}

	public void sectorizarEstaciones()
	{
		ListIterator<Station> ite = listaStations.iterator();
		while(ite.hasNext())
		{
			Station actual = ite.next();
			double lat = actual.getLat();
			double longi = actual.getLongitude();
			Sector enSecto = enSector(longi,lat);

			if(enSecto!=null)
			{
				boolean hay = enSecto.hayStation(actual);
				if(!hay)
				{
					enSector(longi,lat).addStation(actual);
				}
			}
		}
	}
	/**
	 * Revisa si un par de coordenadas está en un sector y retorna este mismo
	 * @param longi Coordenadas en longitud
	 * @param lati Coordenadas en latitud
	 * @return Sector con las coordenadas, null si no se encuentra
	 */
	public Sector enSector(double longi, double lati)
	{
		Sector r = null;
		for(int i=0;i<xSectores;i++)
		{
			for(int j=0;j<ySectores;j++)
			{
				Sector actual = sectores[i][j];
				double longInicial = actual.longInicial();
				double longFinal = actual.longFinal();
				double latInicial = actual.latInicial();
				double latFinal = actual.latFinal();
				if(longInicial<=longi && longFinal>=longi)
				{
					if(latInicial<=lati && latFinal>=lati)
					{
						r=actual;
					}
				}
			}
		}
		return r;
	}

	public void cargarHashEstaciones()
	{
		ListIterator<Trip> ite = listaTrips.iterator();
		while(ite.hasNext())
		{
			Trip tripcito = ite.next();
			String ini = tripcito.startStationName();
			String fini = tripcito.endStationName();
			String nombre = ini+"-"+fini;
			RedBlackBST<Integer, LinkedList<Trip>> busqueda = hashEstaciones.get(nombre);
			if(busqueda != null)
			{
				int duracion = tripcito.getTripDuration();
				LinkedList<Trip> listita = busqueda.get(duracion);
				if(listita==null)
				{
					listita=new LinkedList<Trip>();
				}
				listita.add(tripcito);
				hashEstaciones.get(nombre).put(duracion, listita);

			} else
			{
				LinkedList<Trip> nuevaLista = new LinkedList<Trip>();
				nuevaLista.add(tripcito);
				RedBlackBST<Integer, LinkedList<Trip>> arbolNuevo=new RedBlackBST<Integer, LinkedList<Trip>>();
				arbolNuevo.put(tripcito.getTripDuration(), nuevaLista);
				hashEstaciones.put(nombre, arbolNuevo);
			}

		}
	}

	//METODO CREADO POR Maria Jose Ocampo Vargas
	//METODO CREADO POR Maria Jose Ocampo Vargas
	//METODO CREADO POR Maria Jose Ocampo Vargas

	public static double calculateDistance (double lat1, double lon1, double longitudReferencia, double latitudReferencia) {

		final int R = 6371*1000; // Radious of the earth in meters 

		double latDistance = Math.toRadians(latitudReferencia-lat1); 

		double lonDistance = Math.toRadians(longitudReferencia-lon1); 

		double a = Math.sin(latDistance/2) * Math.sin(latDistance/2) + Math.cos(Math.toRadians(lat1))* Math.cos(Math.toRadians(latitudReferencia)) * Math.sin(lonDistance/2) * Math.sin(lonDistance/2); 

		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 

		double distance = R * c;

		return distance; 

	}


}
