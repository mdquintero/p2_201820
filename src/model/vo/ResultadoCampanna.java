package model.vo;

import model.data_structures.LinkedList;

public class ResultadoCampanna{
	public ResultadoCampanna(int costo, LinkedList<Station> stations)
	{
		costoTotal=costo;
		estaciones = stations;
	}
	public int costoTotal;
	public LinkedList<Station> estaciones;
	
	public void addCosto(double costo)
	{
		costoTotal+=costo;
	}
	
	public void addToEstaciones(Station st)
	{
		estaciones.add(st);
	}
}