package model.vo;

public class Route {

	private String id;

	private String tipo; 

	private String[] ruta;

	private String calleReferencia;

	private String calleLimiteExtremo1;

	private String calleLimiteExtremo2;

	private double longitud;

	private int sector;
	
	private double recorridoRuta;
	
	private double distanciaC5;

	public Route(String ID,String pTipo, String[] pRuta, String pCalleReferencia, String pCalleLimiteExtremo1, String pCalleLimiteExtremo2, double pLongitud)
	{
		tipo = pTipo;
		ruta = pRuta;
		calleReferencia = pCalleReferencia;
		calleLimiteExtremo1 = pCalleLimiteExtremo1;
		calleLimiteExtremo2 = pCalleLimiteExtremo2;
		longitud = pLongitud;
		sector = 0;
		recorridoRuta = 0;
		id = ID;

	}
	

	
	public double getRecorrido(){
		return recorridoRuta;
	}
	
	public void addRecorrido(double param)
	{
		recorridoRuta+=param;
	}
	public String getID()
	{
		return id;
	}

	public void setID(String id)
	{
		this.id=id;
	}
	public String getTipo()
	{
		return tipo;
	}

	public void setTipo(String tipo) 
	{
		this.tipo = tipo;
	}

	public String[] getRuta()
	{
		return ruta;
	}

	public void setRuta(String[] ruta) 
	{
		this.ruta = ruta;
	}

	public String getCalleReferencia()
	{
		return calleReferencia;
	}

	public void setCalleReferencia(String calleReferencia) 
	{
		this.calleReferencia = calleReferencia;
	}

	public String getCalleLimiteExtremo1() 
	{
		return calleLimiteExtremo1;
	}

	public void setCalleLimiteExtremo1(String calleLimiteExtremo1) 
	{
		this.calleLimiteExtremo1 = calleLimiteExtremo1;
	}

	public String getCalleLimiteExtremo2()
	{
		return calleLimiteExtremo2;
	}

	public void setCalleLimiteExtremo2(String calleLimiteExtremo2)
	{
		this.calleLimiteExtremo2 = calleLimiteExtremo2;
	}

	public double getLongitud()
	{
		return longitud;
	}

	public void setLongitud(double longitud)
	{
		this.longitud = longitud;
	}

	public int getSector()
	{
		return sector;
	}
	public double latInicial()
	{
		double r = 0.0;
		String inicial = ruta[1];
		String[] pareja = inicial.split(" ");
		if(pareja.length==3)
		{
			r = Double.parseDouble(pareja[2]);
		} else if(pareja.length==2)
		{
			r = Double.parseDouble(pareja[1]);
		}
		return r;
	}
	
	public double[] latitudes()
	{
		double[] r = new double[ruta.length];
		for(int i = 0; i<ruta.length;i++)
		{
			String actual = ruta[i];
			String[] pareja = actual.split(" ");
			if(pareja.length==3)
			{
				r[i] = Double.parseDouble(pareja[2]);
			} else if(pareja.length==2)
			{
				r[i] = Double.parseDouble(pareja[1]);
			}
		}
		return r;
	}
	
	public double[] longitudes()
	{
		double[] r = new double[ruta.length];
		for(int i=0;i<ruta.length;i++)
		{
			String actual = ruta[i];
			String[] pareja = actual.split(" ");
			if(pareja.length==3)
			{
				r[i] = Double.parseDouble(pareja[1]);
			} else if(pareja.length==2)
			{
				r[i] = Double.parseDouble(pareja[0]);
			}
		}
		return r;
	}
	
	public double latFinal()
	{
		double r = 0.0;
		String fin = ruta[ruta.length-1];
		String[] pareja = fin.split(" ");
		if(pareja.length==3)
		{
			r = Double.parseDouble(pareja[2]);
		} else if(pareja.length==2)
		{
			r = Double.parseDouble(pareja[1]);
		}
		return r;
	}
	
	public double longInicial()
	{
		double r = 0.0;
		String inicial = ruta[1];
		String[] pareja = inicial.split(" ");
		if(pareja.length==3)
		{
			r = Double.parseDouble(pareja[1]);
		} else if(pareja.length==2)
		{
			r = Double.parseDouble(pareja[0]);
		}
		return r;
	}
		
	public double longFinal()
	{
		double r = 0.0;
		String fin = ruta[ruta.length-1];
		String[] pareja = fin.split(" ");
		if(pareja.length==3)
		{
			r = Double.parseDouble(pareja[1]);
		} else if(pareja.length==2)
		{
			r = Double.parseDouble(pareja[0]);
		}
		return r;
	}
	
	public void setSector(int sector)
	{
		this.sector=sector;
	}

	public void setDistanciaC5(double distance)
	{
		distanciaC5 = distance;
	}

	public double getDistanciaC5()
	{
		return distanciaC5;
	}
}
