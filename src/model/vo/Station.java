package model.vo;

import java.time.LocalDateTime;

public class Station implements Comparable<Station> {
	private int stationId;
	private String stationName;
	private LocalDateTime startDate;
	private double lat;
	private double longitude;
	private int capacity;
	//TODO Completar

	public Station(int stationId, String stationName, LocalDateTime startDate, double lat, double longitude, int pCapacity) {
		this.stationId = stationId;
		this.stationName = stationName;
		this.startDate = startDate;
		this.longitude = longitude;
		this.lat = lat;
		this.capacity= pCapacity;
	}

	@Override
	public int compareTo(Station o) {
		// TODO Auto-generated method stub
		return 0;
	}

	public LocalDateTime getStartDate() {
		return startDate;
	}

	public int getStationId() {
		return stationId;
	}

	public String getStationName() {
		return stationName;
	}
	
	public double getLat()
	{
		return lat;
	}
	
	
	public double getLongitude()
	{
		return longitude;
	}
	
	public int getCapacity(){
		return capacity;
	}
}
