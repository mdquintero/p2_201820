package model.vo;

import java.time.LocalDateTime;

public class Trip implements Comparable<Trip> {

    public final static String MALE = "male";
    public final static String FEMALE = "female";
    public final static String UNKNOWN = "unknown";
    public final static String SUBSCRIBER = "Subscriber";
    public final static String CUSTOME = "Customer";
    
    public enum Tipo
    {
    	SUBSCRIBER,CUSTOMER;
    }
    
    private Tipo tipo;
    private int tripId;
    private LocalDateTime startTime;
    private LocalDateTime stopTime;
    
    private int bikeId;
    private int tripDuration;
    private int startStationId;
    private int endStationId;
    private String gender;
    private double distance;
    private String estacionInicio;
    private String estacionFinal;

    
    public Trip(int tripId, LocalDateTime startTime, LocalDateTime stopTime, int bikeId, 
    		int tripDuration, int startStationId, int endStationId, String gender, Tipo tipo) {
        this.tripId = tripId;
        this.startTime = startTime;
        this.stopTime = stopTime;
        this.bikeId = bikeId;
        this.tripDuration = tripDuration;
        this.startStationId = startStationId;
        this.endStationId = endStationId;
        this.gender = gender;
        distance = 0;
        this.tipo = tipo;
    }


    @Override
    public int compareTo(Trip o) {
    	
    	
    	
    		if(this.startTime.isBefore(o.stopTime))
    			return -1;
    		else if(this.startTime.isAfter(o.stopTime))
    			return 1;
    		else
    			return 0;
    	
    	}    
    
    public int getTripId() {
        return tripId;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public LocalDateTime getStopTime() {
        return stopTime;
    }

    public int getBikeId() {
        return bikeId;
    }

    public int getTripDuration() {
        return tripDuration;
    }

    public int getStartStationId() {
        return startStationId;
    }

    public int getEndStationId() {
        return endStationId;
    }

    public String getGender() {
        return gender;
    }
    
    public double getDistance()
    {
    	return distance;
    }
    
    public void setDistance(double pDistance) {
    	distance = pDistance;
    }
    
    public void setTipo(Tipo t)
    {
    	this.tipo=t;
    }
    
    public Tipo getTipo()
    {
    	return tipo;
    }
    
    public String startStationName()
    {
    	return this.estacionInicio;
    }
    
    public String endStationName()
    {
    	return this.estacionFinal;
    }
    
    public void setStart(String pInicio) {
    	this.estacionInicio=pInicio;
    }
    public void setEnd(String pEnd) {
    	this.estacionFinal=pEnd;
    }
}
